% *************************************************************************
%
% function evntVal = JumpSet(y, z, p)
%
% This MATLAB function defines the occurrence of discrete events that 
% change the dynamics of a passive dynamic biped in 2D. The model's current
% continuous and discrete states together with the model parameters are
% provided by the calling routine to which a vector of event function
% values is returned. The directional zero-crossings of these functions
% trigger each a different event. The function 'exctFcnHndl' (with
% parameters 's') describes the excitation inputs for the actuators of 
% the system. If it is not provided, a purely passive system is simulated. 
% 
%
% Input:  - A vector of continuous states 'y' 
%         - A vector of discrete states 'z' 
%         - A vector of model system parameters 'p'
%         OPTIONAL:
%           - An excitation function 'exctFcnHndl', with the syntax  
%             u = ExcitationFunction(y, z, s), describing the active inputs
%             to the system.  If this function is not provided, the inputs
%             are considered static and drawn from the definition file 
%             (ExctStateDefinition).
%           - A vector of parameters 's' for this function
%
% Output: - Each entry of 'evntVal' corresponds to a function, of which a
%           zero-crossing (with positive derivative) is detected as event
%
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, COMPUTEDIFFERENTIABLEFORCES 
%            CONTSTATEDEFINITION, DISCSTATEDEFINITION, SYSTPARAMDEFINITION,
%            EXCTSTATEDEFINITION, EXCTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC, 
%            SYMBOLICCOMPUTATIONOFEQM. 
%
function evntVal = JumpSet(y, ~, p)
   
    % Get a mapping for the state vector.
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~,            ~, contStateIndices] = ContStateDefinition();
    end
    
    % Event 1: Detect touchdown (this is also the final event)
    n_events = 1;
    evntVal = zeros(n_events,1);
    
    % *******
    % Event 1: Detect touchdown 
    if y(contStateIndices.alpha)>0 %(only if swing leg is in front of stance leg)
        evntVal(1) = -ContactDynamicsWrapper(y, p) + 1e-12; 
        %plot(y(contStateIndices.alpha),ContactDynamicsWrapper(y, p),'r*')
    else
        evntVal(1) = 1;
    end
end
% *************************************************************************
% *************************************************************************