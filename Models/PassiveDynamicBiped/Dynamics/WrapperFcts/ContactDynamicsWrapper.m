% *************************************************************************
%
% function [contHeight, J_cont, M_cont] = ContactDynamicsWrapper(y, p)
%
% This MATLAB function calls the automatically generated function code that
% computes the contact jacobian and the associated mass matrix.  This
% function is not called directly to reflect the definition of the
% continuous states 'y'.  It is written for the model of a passive dynamic
% biped.   
%
% Input:  - The height of the contact point above the ground 'contHeight'
%         - A vector of continuous states 'y'
%         - A vector of model system parameters 'p'
% Output: - The contact jacobian 'J_cont'
%         - The contact mass matrix 'M_cont'
%
% NOTE:  These to matrices are given for an extended set of coordinates
%       (x,y,gamma,alpha), and not for minimal coordinates.
%
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a 
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also SYMBOLICCOMPUTATIONOFEQM, CONTSTATEDEFINITION,
%            SYSTPARAMDEFINITION. 
%
function [contHeight, J_cont, M_cont] = ContactDynamicsWrapper(y, p)
    % Map the generalized coordinates:
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
    end
    gamma = y(contStateIndices.gamma);
    alpha = y(contStateIndices.alpha);
    
    % Map the system parameters:
    % Keep the index-structs in memory to speed up processing
    persistent systParamIndices
    if isempty(systParamIndices)
        [~, ~, systParamIndices] = SystParamDefinition();
    end
    gx    = p(systParamIndices.gx);
    gy    = p(systParamIndices.gy);
    l_0   = p(systParamIndices.l_0);
    m1    = p(systParamIndices.m1);
    m2    = p(systParamIndices.m2);
    l2x   = p(systParamIndices.l2x);
    l2y   = p(systParamIndices.l2y);
    rFoot = p(systParamIndices.rFoot);
    j2    = p(systParamIndices.j2);
    
    contHeight = ContactHeight(gamma,alpha,l_0,l2x,l2y,rFoot,gx,gy,m1,m2,j2);

    if nargout>1
        % Compute the depended coordinates (this is not really necessary, as
        % neither the mass matrix nor the contact jacobian depend on these
        % values):  
        x  =  - gamma*rFoot - sin(gamma)*(l_0 - rFoot);
        y_ =          rFoot + cos(gamma)*(l_0 - rFoot);

        M_cont = ContactMassMatrix(x,y_,gamma,alpha,l_0,l2x,l2y,rFoot,gx,gy,m1,m2,j2);
        J_cont = ContactJacobian(x,y_,gamma,alpha,l_0,l2x,l2y,rFoot,gx,gy,m1,m2,j2);
    end
end