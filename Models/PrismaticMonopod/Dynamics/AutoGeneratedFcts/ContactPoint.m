function cont_point = ContactPoint(x,y,phi,alpha,l,l2,l3,rFoot,g,m1,m2,m3,j1,j2,j3)
%CONTACTPOINT
%    CONT_POINT = CONTACTPOINT(X,Y,PHI,ALPHA,L,L2,L3,RFOOT,G,M1,M2,M3,J1,J2,J3)

%    This function was generated by the Symbolic Math Toolbox version 5.4.
%    10-Apr-2011 10:50:09

t811 = alpha+phi;
cont_point = [x+l.*sin(t811);-rFoot+y-l.*cos(t811)];
