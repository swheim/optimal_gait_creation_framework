<<<<<<< .mine
%************************************************************************
=======
%************************************************************************
%
% Main_Monopod1D - script 
>>>>>>> .r20
%
% Main_SLIP- script 
%
% This MATLAB script finds optimal solutions for a simple SLIP (Spring
% Loaded Inverted Pendulum) model in 2D.
% In particular, it does:  
%
% - (a) Set up the framework and include all necessary files, folders, and
%       variables  
% - (b) Run some basic simulations while creating different forms of
%       output.
% - (c) Methods to create a periodic running motion.
% - (d) Run a first order stability analysis of the periodic motion
% - (e) Analyze the reaction to disturbances of zero energy
% - (f) Simulate a couple of steps
% - (g) Implement a Raibert-style controller on the SLIP model
%
% Input:  - NONE
% Output: - NONE
%
%
% Created by C. David Remy on 07/10/2011
% MATLAB 2010a - Windows - 64 bit
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, JUMPSET,
%            SYMBOLICCOMPUTATIONOFEQM, CONTSTATEDEFINITION,
%            DISCSTATEDEFINITION, SYSTPARAMDEFINITION, EXCTTATEDEFINITION,
%            EXCTPARAMDEFINITION, COMPUTEDIFFERENTIABLEFORCES,
%            EXCITATIONFUNCTION 
%
%************************************************************************


%% (a) Initial Setup
% Define the necessary directories, so that all library-files of the
% framework and the correct model files can be accessed.  Retrieve all
% initial model states and parameters from the state definition files.

% Make a clean sweep:
clear all
close all
clc
% Define a base directory to make this file easily portable to other computers:
GaitCreationDir = 'C:\GaitCreation\';
if ~exist(GaitCreationDir,'dir')
    error('SummerschoolSLIP:Main:GaitCreationDirectorNotExist', 'The specified GaitCreation-directory was not found on your computer.  Please adjust this path to match the installiation on your computer')
end
if (isunix)
    slash = '/';
else
    slash = '\';
end
cd([GaitCreationDir, slash, 'Models', slash, 'Summerschool - SLIP running']);
% Reset the MATLAB search path to its default value:
path(pathdef);
% Set the path to include all library functions:
path(path,[GaitCreationDir,slash,'Shared;',...
           GaitCreationDir,slash,'Shared',slash,'Analysis;',...
           GaitCreationDir,slash,'Shared',slash,'Graphics',slash,'Misc;',...
           GaitCreationDir,slash,'Shared',slash,'Graphics',slash,'SeriesElasticActuation;',...
           GaitCreationDir,slash,'Shared',slash,'Graphics;',...
           GaitCreationDir,slash,'Shared',slash,'Utilities;',...
           GaitCreationDir,slash,'Shared',slash,'Synthesis;']);
% Set the path to include the model specific functions:
% (Every time a different model is processed, it is important to check that
% the path only includes the directories of the current model)
path(path,[GaitCreationDir,slash,'Models',slash,'Summerschool - SLIP running;',...
           GaitCreationDir,slash,'Models',slash,'Summerschool - SLIP running',slash,'Dynamics;',...
           GaitCreationDir,slash,'Models',slash,'Summerschool - SLIP running',slash,'Dynamics',slash,'Definitions;',...
           GaitCreationDir,slash,'Models',slash,'Summerschool - SLIP running',slash,'Graphics;']);

% Get the basic state and parameter values, their names, and the
% corresponding index mapping.  By this we can access the vectors by name,
% which keeps the structure very general but allows a clear indexing.
[contStateVec, contStateNames, contStateIndices] = ContStateDefinition();
[discStateVec, discStateNames, discStateIndices] = DiscStateDefinition();
[systParamVec, systParamNames, systParamIndices] = SystParamDefinition();


%% (b) Basic simulation with various output-options
% Define a set of initial state and parameter vectors, and call the basic
% simulation functionality.  Various output options are introduced.

% Initial values for continuous and discrete states, as well as for the
% system parameters are copied from the basic state definitions: 
yIN = ContStateDefinition;
zIN = DiscStateDefinition;
p   = SystParamDefinition;
% Define additional options for the simulation:
simOptions.tIN  = 0;  % The simulation will start at t = 0
simOptions.tMAX = 5;  % The simulation will abort when t reaches 5.  This prevents an infinite simulation-loop, when the terminal event is missed.

% Simulate one full stride (which ends when the swing foot strikes the ground)
[yOUT, zOUT, tOUT] = HybridDynamics(yIN, zIN, p, simOptions);

% Some more output options:
    % The states of both simulations are now plotted while simulating.  The
    % class is created with the flag 'false', as it's simulating a purely
    % passive system:  
    plotOUTPUT = PlotStateCLASS(false);
    HybridDynamics(yIN, zIN, p, plotOUTPUT, simOptions);
    
    % Similarly, we can record the states for later use:
    recOUTPUT = RecordStateCLASS();
    [yOUT, zOUT, tOUT, recOUTPUT] = HybridDynamics(yIN, zIN, p, recOUTPUT, simOptions);
    simRES = recOUTPUT.retrieve();
    figure('Name','SLIP model: y and dy of in-place hopping','WindowStyle','docked')
    grid on; hold on; box on;
    % Define which states are plotted:
    plotStates = [contStateIndices.y , contStateIndices.dy];
    plot(simRES.t,simRES.continuousStates(plotStates,:))
    legend(simRES.continuousStateNames(plotStates));
    
    % A graphical output can be linked as well:
    graphOUTPUT = SLIP_Model_Graphics(p);
    HybridDynamics(yIN, zIN, p, graphOUTPUT, simOptions);

% Let's add some forward motion to the initial states:
yIN(contStateIndices.dx) = 1;
[yOUT, zOUT, tOUT] = HybridDynamics(yIN, zIN, p, graphOUTPUT, simOptions);

% Since this is obviously not periodic, we need to change the angle of
% attack:
p(systParamIndices.angAtt) = 0.3;  % Gives a roughly periodic motion
graphOUTPUT = SLIP_Model_Graphics(p); % Must be called again with new parameters p, such that the new angle of attack is visualized
[yOUT, zOUT, tOUT] = HybridDynamics(yIN, zIN, p, graphOUTPUT, simOptions);



%% (c) Create a periodic gait
% (i) Find the right forward speed
% For a SLIP model, the problem of gait creation can be defined in two
% ways.  One is to find periodic initial conditions that match the given
% angle of attack. I.e., we look for states in which the model is started
% and that are reached again at the end of one stride.  It turns out that
% it is sufficent to only alter the forward velocity. 
%
% The basic state-definitions are used to get an initial guess for what the
% periodic states could be:  
yINIT = ContStateDefinition;
zINIT = DiscStateDefinition;
pINIT = SystParamDefinition;
% Define a specific angle of attack:
pINIT(systParamIndices.angAtt) = pi/8;
% The following arrays define which states and parameters can be altered in
% the root-search.  States that are set to '0', will remain at their
% initial guess:  
yOPTIM(contStateIndices.x)  = 0;  % Always start at x = 0;
yOPTIM(contStateIndices.dx) = 1;  % The correct forward velocity is found by the root-search
yOPTIM(contStateIndices.y)  = 0;  % Always start at y = 1.2;
yOPTIM(contStateIndices.dy) = 0;  % Since we always stop at apes transit, we should always start at apex transit
zOPTIM = zeros(size(zINIT));  % No discrete states are altered
pOPTIM = zeros(size(pINIT));  % No parameters are altered
% Define which states must be periodic. Again, this is indicated by a '1'
% in the corresponding array:
yPERIOD(contStateIndices.x)  = 0;  % Forward motion is not periodic;
yPERIOD(contStateIndices.dx) = 1;  % Forward speed must be periodic
yPERIOD(contStateIndices.y)  = 1;  % Hopping height must be periodic
yPERIOD(contStateIndices.dy) = 0;  % Since we always start and stop at apex transit, this is fullfilled automatically
zPERIOD = zeros(size(zINIT));  % No periodicity for discrete states
% An upper limit for the stride duration is set, such that the simulation
% will be aborted if the terminal state is never reached.  In this case, an
% error message will be created:  
solveOptions.tMAX = 5;
% Call the root-search function.
[yCYC, zCYC, pCYC] =  FindPeriodicSolution(@HybridDynamics, yINIT,   zINIT,  pINIT,... 
                                                            yOPTIM,  zOPTIM, pOPTIM,... 
                                                            yPERIOD, zPERIOD, ...
                                                            solveOptions);
disp(['The forward velocity of the periodic gait is: ',num2str(yCYC(contStateIndices.dx))]);
% Simulate solution:
graphOUTPUT = SLIP_Model_Graphics(pCYC); % Must be called again with new parameters p, such that the new angle of attack is visualized
[yOUT, zOUT, tOUT] = HybridDynamics(yCYC, zCYC, pCYC, graphOUTPUT, simOptions);
%
%
%% (ii) Find the right angle of attack
% In the second attemped, we define a forward speed, and search for the
% correct angle of attack via adaptation of the parameter vector p
%
% The basic state-definitions are the same:  
yINIT = ContStateDefinition;
zINIT = DiscStateDefinition;
pINIT = SystParamDefinition;
% But this time, we define a forward velocity:
% yINIT(contStateIndices.dx) = 1;   % unstable
yINIT(contStateIndices.dx) = 2.5; % stable
% Currently the initial guess for the angle of attack is pointing straight
% downwards.  Let's help the optimization with a better initial guess:
pINIT(systParamIndices.angAtt) = pi/8;
% Define which states and parameters can be altered in the root-search:
yOPTIM = zeros(size(yINIT));  % This time, we don't want to alter the initial continuous states
zOPTIM = zeros(size(zINIT));  % No discrete states are altered
% Instead, we alter the angle of attack
pOPTIM = zeros(size(pINIT));  % First set all flags to 0.
pOPTIM(systParamIndices.angAtt) = 1; % Set the flag for the angle of attack to '1'
% We require the same states to be periodic:
yPERIOD(contStateIndices.x)  = 0;  % Forward motion is not periodic;
yPERIOD(contStateIndices.dx) = 1;  % Forward speed must be periodic
yPERIOD(contStateIndices.y)  = 1;  % Hopping height must be periodic
yPERIOD(contStateIndices.dy) = 0;  % Since we always start and stop at apex transit, this is fullfilled automatically
zPERIOD = zeros(size(zINIT));  % No periodicity for discrete states
% Same options for the solver::  
solveOptions.tMAX = 5;
% Call the root-search function. 
[yCYC, zCYC, pCYC] =  FindPeriodicSolution(@HybridDynamics, yINIT,   zINIT,  pINIT,... 
                                                            yOPTIM,  zOPTIM, pOPTIM,... 
                                                            yPERIOD, zPERIOD, ...
                                                            solveOptions);
disp(['The required angle of attack of the periodic gait is: ',num2str(pCYC(systParamIndices.angAtt))]);
% Simulate solution:
graphOUTPUT = SLIP_Model_Graphics(pCYC); % Must be called again with new parameters p, such that the new angle of attack is visualized
[yOUT, zOUT, tOUT] = HybridDynamics(yCYC, zCYC, pCYC, graphOUTPUT, simOptions);




%% (d) Stability analysis
% The stability of the linearized system is evaluated by computing the
% Floquet multipliers (Eigenvalues of the Monodromy matrix)

% Arrays define which states are subject to analysis:
yANALYS(contStateIndices.x)  = 0;  % x is not periodic
yANALYS(contStateIndices.dx) = 1;  % All states are analyzed!
yANALYS(contStateIndices.y)  = 1;
yANALYS(contStateIndices.dy) = 1;
zANALYS = [];                      % No discrete states are analyzed
% The eigenvalues and eigenvectors are computed numerically:
[eigenValuesCYC, eigenVectorsCYC] =  FloquetAnalysis(@HybridDynamics, yCYC,    zCYC,   pCYC,... 
                                                                      yANALYS, zANALYS);

                                                                  
%% (e) Disturbance of zero energy:
% We now vary the forward velocity dx and the hopping height y, such that
% the energy content of the system das not change
% Compute the nominal amount of energy of this gait:
E_nom = 0.5*pCYC(systParamIndices.m_0)*yCYC(contStateIndices.dx)^2 + pCYC(systParamIndices.m_0)*pCYC(systParamIndices.g)*yCYC(contStateIndices.y);
% number of sample points
n = 50;
% Variations in forward velocity:
dx_in = linspace(2.3, 2.6, n);
% Energy equivalent change in hopping height:
y_in  = E_nom - (0.5*dx_in.^2)./pCYC(systParamIndices.g);
dx_out = zeros(1,n);
y_out = zeros(1,n);
for i = 1:n;
    disp(['Iteration: ',num2str(i)])
    yIN = yCYC;
    yIN(contStateIndices.dx) = dx_in(i);
    yIN(contStateIndices.y) = y_in(i);
    yOUT = HybridDynamics(yIN, zCYC, pCYC, simOptions);
    dx_out(i) = yOUT(contStateIndices.dx);
    y_out(i)  = yOUT(contStateIndices.y);
end
% Plot the first order return map of the hopper as a function of dx:
figure
hold on
grid on
box on
plot(dx_in, dx_out)
% All periodic solutions are on this line:
line(dx_in, dx_in, 'color', 'k')
axis equal
axis tight
xlabel('dx before stride')
ylabel('dx after stride')
%
% The same figure for hopping height y.  The two figures are equivalent
figure
hold on
grid on
box on
plot(y_in, y_out)
% All periodic solutions are on this line:
line(y_in, y_in, 'color', 'k')
axis equal
axis tight
xlabel('y before stride')
ylabel('y after stride')




%% (f) Compare a couple of steps that were started closely to the second (unstable) fixed point:
% (i) starting inside the basin of attraction:
i = 10;
% % (ii) starting outside the basin of attraction:
% i = 9;
% Initialize simulation
yIN = yCYC;
yIN(contStateIndices.dx) = dx_in(i);
yIN(contStateIndices.y) = y_in(i);
recOUTPUT = RecordStateCLASS();
zIN = zCYC;
tOUT = 0;

% We simulate one stride after the other, until the simulation returns tOUT
% = -1, which indicates that the simulation stoped not because of the
% terminal event, but because it ran out of time.
while tOUT ~= -1
    % Define additional options for the simulation:
    simOptions.tIN  = tOUT;  % The simulation will start at t = 0
    simOptions.tMAX = 100;  % The simulation will abort when t reaches 5.  This prevents an infinite simulation-loop, when the terminal event is missed.
    [yOUT, zOUT, tOUT, recOUTPUT] = HybridDynamics(yIN, zIN, pCYC, recOUTPUT, simOptions);
    yIN = yOUT;
    zIN = zOUT;
end

% After simulation the results can be retrieved from the output object.
% Here they are used for selective plotting of the leg angles:
simRES = recOUTPUT.retrieve();
figure('Name','SLIP model: dx and y of forward hopping','WindowStyle','docked')
grid on; 
hold on; 
box on;
% Define which states are plotted:
plotStates = [contStateIndices.dx , contStateIndices.y];
plot(simRES.t,simRES.continuousStates(plotStates,:))
legend(simRES.continuousStateNames(plotStates));
axis([0, 100, 0, 3])



%% (g) Raibert-style controller:
% Run a simulation for hopping in place, to get an estimate of the contact
% time:
simOptions.tIN  = 0;  % The simulation will start at t = 0
simOptions.tMAX = 5;  % The simulation will abort when t reaches 5.  This prevents an infinite simulation-loop, when the terminal event is missed.
recOUTPUT = RecordStateCLASS();
[yOUT, zOUT, tOUT, recOUTPUT] = HybridDynamics(yCYC, zCYC, pCYC, recOUTPUT, simOptions);
simRES = recOUTPUT.retrieve();
figure('Name','SLIP model: phase in-place hopping','WindowStyle','docked')
grid on; hold on; box on;
% Only plot the phase
plotStates = [discStateIndices.phase];
plot(simRES.t,simRES.discreteStates(plotStates,:))
legend(simRES.discreteStateNames(plotStates));
% Contact time is: 0.8566
% Lift-off time is: 1.325
T_stance =  1.325 - 0.8566;

% The following code is identical to section (e), only one line of code is
% added that adaptes the angle of attack before the stride is simulated.
% We now vary the forward velocity dx and the hopping height y, such that
% the energy content of the system das not change
% Compute the nominal amount of energy of this gait:
E_nom = 0.5*pCYC(systParamIndices.m_0)*yCYC(contStateIndices.dx)^2 + pCYC(systParamIndices.m_0)*pCYC(systParamIndices.g)*yCYC(contStateIndices.y);
% number of sample points
n = 10;
% Variations in forward velocity:
dx_in = linspace(2.3, 2.6, n);
% Energy equivalent change in hopping height:
y_in  = E_nom - (0.5*dx_in.^2)./pCYC(systParamIndices.g);
dx_out = zeros(1,n);
y_out = zeros(1,n);
for i = 1:n;
    disp(['Iteration: ',num2str(i)])
    yIN = yCYC;
    yIN(contStateIndices.dx) = dx_in(i);
    yIN(contStateIndices.y) = y_in(i);
    % HERE IS THE RAIBERT STYLE CONTROLLER:
    % The proportional gain on velocity:
    k_dx = 0.1;
    pCTRL = pCYC;
    pCTRL(systParamIndices.angAtt) = asin((dx_in(i)*T_stance)/(2*pCYC(systParamIndices.l_0)) + ( k_dx*(dx_in(i) - 2.5))/pCYC(systParamIndices.l_0) );
   
    yOUT = HybridDynamics(yIN, zCYC, pCTRL, simOptions);
    dx_out(i) = yOUT(contStateIndices.dx);
    y_out(i)  = yOUT(contStateIndices.y);
end
% Plot the first order return map of the hopper as a function of dx:
figure
hold on
grid on
box on
plot(dx_in, dx_out)
% All periodic solutions are on this line:
line(dx_in, dx_in, 'color', 'k')
axis equal
axis tight
xlabel('dx before stride')
ylabel('dx after stride')





