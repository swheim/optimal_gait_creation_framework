% function x_ = getSystemStates(y_)
% Return only the states describing the system, in the order 
% [displacements flows], i.e.
% [x y phi dx dy dphi]
% Note that the continuous state vector y_ also contains such things as
% time and CoT. Future version will probably modify to deliver the vector
% of penalizeable states.


function x = getSystemStates(y_)

    % Map the generalized coordinates:
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
    end
    x      = [y_(contStateIndices.x,:);
               y_(contStateIndices.y,:);
               y_(contStateIndices.phi,:);
               y_(contStateIndices.yF,:);
               y_(contStateIndices.phiH,:);
               y_(contStateIndices.phiT,:);
               y_(contStateIndices.dx,:);
               y_(contStateIndices.dy,:);
               y_(contStateIndices.dphi,:);
               y_(contStateIndices.dyF,:);
               y_(contStateIndices.dphiH,:);
               y_(contStateIndices.dphiT,:);];

end