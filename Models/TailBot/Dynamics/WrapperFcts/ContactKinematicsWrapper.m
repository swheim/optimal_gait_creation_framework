% *************************************************************************
%
% Msc Thesis STEVE HEIM, 2014
%
% function [pos, J, dJdtTIMESdqdt] = ContactKinematicsWrapper(y, p)
%
% This MATLAB function calls the automatically generated function code that
% computes the contact jacobian and the contact point position. The
% generated function is not called directly to reflect the definition of 
% the continuous states 'y'. 
%
% This is written for a prismatic monopod with tail
%
% Input:  - A vector of continuous states 'y'
%         - A vector of model system parameters 'p'
% Output: - The current position of the contact point 'pos'
%         - The contact jacobian 'J' of the system
%         - The time derivative of J multiplied by dqdt 'dJdtTIMESdqdt'
%
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a 
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also SYMBOLICCOMPUTATIONOFEQM, CONTSTATEDEFINITION,
%            SYSTPARAMDEFINITION. 
%
function [pos, J, dJdtTIMESdqdt] = ContactKinematicsWrapper(y_, p)
    % Map the generalized coordinates:
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
    end
    x      = y_(contStateIndices.x);
    y     = y_(contStateIndices.y);
    phi    = y_(contStateIndices.phi);
    yF     = y_(contStateIndices.yF);
    phiH   = y_(contStateIndices.phiH);
    phiT   = y_(contStateIndices.phiT);
    
    % Map the system parameters:
    % Keep the index-structs in memory to speed up processing
    persistent systParamIndices
    if isempty(systParamIndices)
        [~, ~, systParamIndices] = SystParamDefinition();
    end
     g = p(systParamIndices.g);
     lxC = p(systParamIndices.lxC);
     lyC = p(systParamIndices.lyC);
     lxB = p(systParamIndices.lxB);
     lyB = p(systParamIndices.lyB);
     lT = p(systParamIndices.lT);
     mS = p(systParamIndices.mS);
     mF = p(systParamIndices.mF);
     mT = p(systParamIndices.mT);
     jS = p(systParamIndices.jS);
     jF = p(systParamIndices.jF);
     jT = p(systParamIndices.jT);
     kF = p(systParamIndices.kF);
     kF0 = p(systParamIndices.kF0);
     bF = p(systParamIndices.bF);
    
     % q: x y phi yF phiH phiT
     % p: g lxC lyC lxB lyB lT mS mF mT jS jF jT kF kF0 bF
     % parameters to pass to auto-generated functions:
     %   x,y,phi,yF,phiH,phiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF
     
     
    % Call the auto-generated function
    pos  = ContactPoint(x,y,phi,yF,phiH,phiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF);
    if nargout>1 % Jacobian is required
        % Call the auto-generated function
        J    = ContactJacobian(x,y,phi,yF,phiH,phiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF);
    end
    if nargout>2 % Derivative of Jacobian is required
        dx     = y_(contStateIndices.dx);
        dy     = y_(contStateIndices.dy);
        dphi   = y_(contStateIndices.dphi);
        dyF    = y_(contStateIndices.dyF);
        dphiH  = y_(contStateIndices.dphiH);
        dphiT  = y_(contStateIndices.dphiT);
        % Call the auto-generated function
        dJdtTIMESdqdt = ContactJacobianDtTIMESdqdt(x,y,phi,yF,phiH,phiT, dx,dy,dphi,dyF,dphiH,dphiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF);
    end
end